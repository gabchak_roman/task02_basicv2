package com.epam;
import java.util.Scanner;

/**
 * Class fibonacci sequence.
 */
public class Fibonacci {

    /**
     * Sum odd numbers fibonacci sequence.
     */
    private int sumOddNum;
    /**
     * Sum even numbers fibonacci sequence.
     */
    private int sumEvenNum;
    /**
     * Max odd number from fibonacci sequence.
     */
    private int maxOddNumber = 0;
    /**
     * Max even number from fibonacci sequence.
     */
    private int maxEvenNumber = 0;
    /**
     * Odd numbers fibonacci sequence.
     */
    private StringBuffer oddNumbers =new StringBuffer();
    /**
     * Even numbers fibonacci sequence.
     */
    private StringBuffer evenNumbers = new StringBuffer();

    /**
     * Get and check if the input data is a number.
     * @return input number.
     */
    private int inputNumber() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("This is not a number!");
            scanner.next();
        }
        return scanner.nextInt();
    }

    /**
     * Displays the Fibonacci numbers and count the sum Odd and Even numbers.
     */
    private void printFibonacciNumber() {
        System.out.println("Fibonacci sequence\nEnter the number: ");
        int numberIn = inputNumber();
        if (numberIn <= 0) {
            System.out.println(0);
        }
        int number0 = 1;
        int number1 = 1;
        int sum;
        System.out.print("Fibonacci sequence: ");
        for (int i = 0; i < numberIn; i++) {
            sum = i < 2 ? 1 : number0 + number1;
            number0 = number1;
            number1 = sum;
            if (sum % 2 == 0) {
                maxEvenNumber = Math.max(maxEvenNumber, sum);
                oddNumbers.append(" " + sum);
                sumEvenNum += sum;
            } else {
                maxOddNumber = Math.max(maxOddNumber, sum);
                evenNumbers.append(" " + sum);
                sumOddNum += sum;
            }
            System.out.print(sum + " ");
        }
        System.out.println();
    }

    /**
     * Count and displays the percentage
     * of odd and even fibonacci numbers.
     */
    private void printPercentageOfOddAndEvenNumbers() {
        float sumOddAndEvenNum = sumOddNum + sumEvenNum;
        final float PERCENT = 100;

        float percentOfOddNumbers = (sumOddNum / sumOddAndEvenNum) * PERCENT;
        float percentOfEvenNumbers = (sumEvenNum / sumOddAndEvenNum) * PERCENT;

        System.out.println("Odd numbers: " + percentOfOddNumbers + " %");
        System.out.println("Even numbers : " + percentOfEvenNumbers + " %");
    }

    /**
     * Print max odd and even numbers.
     */
    private void printMaxOddAndEvenNum() {
        System.out.println("Max odd number: " + maxOddNumber);
        System.out.println("Max even number: " + maxEvenNumber);
    }

    /**
     * Print odd and even numbers.
     */
    private void printMaxOddAndEvenNumbers() {
        System.out.println("Odd numbers: " + oddNumbers);
        System.out.println("Even numbers: " + evenNumbers);
    }


    /**
     * Starts the program.
     */
    final void run() {
        printFibonacciNumber();
        printMaxOddAndEvenNumbers();
        printMaxOddAndEvenNum();
        printPercentageOfOddAndEvenNumbers();
    }
}
