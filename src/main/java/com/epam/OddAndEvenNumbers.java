package com.epam;

import java.util.Scanner;

/**
 * * Class odd and even numbers.
 */
public class OddAndEvenNumbers {
    /**
     * Interval form.
     */
    private int fromInputNumber;
    /**
     * Interval to.
     */
    private int toInputNumber;
    /**
     * Sum of odd numbers.
     */
    private int sumOfOddNumbers;
    /**
     * Sum of even numbers.
     */
    private int sumOfEvenNumbers;
    /**
     * Сhecks if the entered data is a number.
     *
     * @return input number.
     */
    private int inputCheck() {
        Scanner scanner = new Scanner(System.in);
        while (!scanner.hasNextInt()) {
            System.out.println("This is not a number!");
            scanner.next();
        }
        return scanner.nextInt();
    }

    /**
     * Take input numbers from the keyboard.
     */
    private void getInputNumbers() {
        System.out.println("Odd and even numbers");
        System.out.println("Enter interval");
        System.out.print("from: ");
        fromInputNumber = inputCheck();
        System.out.print("to: ");
        toInputNumber = inputCheck();
    }

    /**
     * Count the sum odd and even numbers.
     */
    private void countSumOddAndEvenNumbers() {
        for (int i = fromInputNumber + 1; i <= toInputNumber; i++) {
            if (i % 2 == 0) {
                sumOfEvenNumbers += i;
            } else {
                sumOfOddNumbers += i;
            }
        }
    }

    /**
     * Displays the sum odd and even numbers.
     */
    private void printSumOddAndEvenNumbers() {
        countSumOddAndEvenNumbers();
        System.out.println("\nSum of odd numbers: " + sumOfOddNumbers);
        System.out.println("Sum of even numbers: " + sumOfEvenNumbers);
    }

    /**
     * Displays odd numbers.
     */
    private void printEvenNumbers() {
        System.out.print("\nEven number output in descending order: \n");
        for (int i = toInputNumber; i >= fromInputNumber; i--) {
            if (i % 2 == 0) {
                System.out.print("[" + i + "] ");
            }
        }
    }

    /**
     * Displays odd numbers.
     */
    private void printOddNumbers() {
        System.out.print("\nOdd numbers output in ascending order: \n");
        for (int i = fromInputNumber; i <= toInputNumber; i++) {
            if (i % 2 != 0) {
                System.out.print("[" + i + "] ");
            }
        }
    }

    /**
     * Starts the program.
     */
    final void run() {
        getInputNumbers();
        printOddNumbers();
        printEvenNumbers();
        printSumOddAndEvenNumbers();
    }
}
