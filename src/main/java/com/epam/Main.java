package com.epam;
/**
 * Main program class.
 */
public class Main {
    /**
     * Starting point.
     * @param args required string array.
     */
    public static void main(final String[] args) {
        OddAndEvenNumbers oddAndEvenNumbers = new OddAndEvenNumbers();
        oddAndEvenNumbers.run();

        System.out.println();

        Fibonacci fibonacci = new Fibonacci();
        fibonacci.run();
    }
}
